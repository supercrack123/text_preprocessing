 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             2 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
 
<line> REV. NO. REV. DATE REVISED PAGE REVISION DESCRIPTIONS 
<line> B1 04 Jun.13 4, 6-8, 13, 14 Issued for COMPANY Review, General Revision 
<line> 0 10 Jul.13 - Issued for IGD-E FEED 
    
    
    
    
    
    
    
    
    
    
    
 
 
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             3 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> Table of Content 
<Contents> 1. SCOPE ..................................................................................................................................................... 4 
<Contents> 2. REGULATIONS, CODES AND STANDARDS.......................................................................................... 4 
<Contents> 2.1 Regulations .................................................................................................................................. 4 
<Contents> 2.2 Equipment Codes and Standards................................................................................................. 4 
<Contents> 2.3 ASME Code Stamping.................................................................................................................. 4 
<Contents> 2.4 Miscellaneous Standards.............................................................................................................. 5 
<Contents> 2.5 Project Specifications ................................................................................................................... 6 
<Contents> 2.6 Order of Precedence .................................................................................................................... 6 
<Contents> 3. UNIT OF MEASUREMENT....................................................................................................................... 6 
<Contents> 4. LANGUAGE.............................................................................................................................................. 6 
<Contents> 5. BASIC DESIGN DATA ............................................................................................................................. 7 
<Contents> 5.1 Site Condition ............................................................................................................................... 7 
<Contents> 5.2 Base Elevation (Above Finished Grade)....................................................................................... 9 
<Contents> 6. DETAILED DESIGN DATA..................................................................................................................... 10 
<Contents> 6.1 Pressure Vessel - General.......................................................................................................... 10 
<Contents> 6.2 Shell and tube Type Heat Exchangers ....................................................................................... 11 
<Contents> 6.3 Air Cooled Exchangers............................................................................................................... 12 
<Contents> 6.4 Noise Control.............................................................................................................................. 13 
<Contents> 6.5 Criticality Ratings........................................................................................................................ 13 
<Contents> 6.6 Thermal Insulation accessories. ................................................................................................. 13 
<Contents> 6.7 Fireproofing ................................................................................................................................ 13 
<Contents> 6.8 Shop Painting ............................................................................................................................. 14 
 
 
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             4 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 1. <**> SCOPE 
<line> These design criteria cover the basic design requirements for pressure vessels and heat 
<line> exchangers for the Integrated Gas Development (IGD) Project for Abu Dhabi Gas Liquefaction 
<line> CO.LTD. (ADGAS) (herein after called COMPANY). This specification shall be read in 
<line> conjunction with Pressure vessel specification GD-100-A4C-5-92001 and other specifications 
<P_line> mentioned in paragraph 2.5.  
<line> 2. <**> REGULATIONS, CODES AND STANDARDS  
<line> The design, material, fabrication, testing, and inspection of pressure vessels and heat 
<Comma_line> exchangers shall comply with the requirements of the following regulations, codes and standards, 
<P_line> and all documents referred thereto.  
<line> 2.1 <**> Regulations  
<line> Not applicable   
<line> 2.2 <**> Equipment Codes and Standards  
<line> 2.2.1 <**> Pressure Vessels  
<line> ASME Boiler and Pressure Vessel Code, Section VIII Division 1, Latest Edition and 
<line> Addenda  
<line> 2.2.2 <**> Boilers  
<line> ASME Boiler and Pressure Vessel Code, Section I, Latest Edition and Addenda  
<line> 2.2.3 <**> Shell and Tube Heat Exchangers  
<line> Standards of the Tubular Exchanger Manufacturers Association, 1999, Latest 
<line> Edition, Class R 
<line> API 660, Latest Edition  
<line> 2.2.4 <**> Air Cooled Exchangers   
<line> API 661, 2006, 6th Edition  
<line> 2.2.5 <**> Plate Heat Exchangers  
<line> API 662 Parts 1 & 2, 2006, 1st Edition  
<line> 2.3 <**> ASME Code Stamping  
<line> ASME Code stamping is required for pressure vessels and heat exchangers of which 
<P_line> design pressure is over 15 bar g and also for equipment where criticality rating is 1 or 2.  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             5 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 2.4 <**> Miscellaneous Standards  
<line> 2.4.1 <**> Materials  
<line> 1) <**> Materials for Pressure Retaining Parts  
<line> (1) ASME Boiler and Pressure Vessel Code, Section II  
<line> (2) ASTM Standard  
<line> 2) <**> Materials for Non-Pressure Parts  
<line> (1) ASME Boiler and Pressure Vessel Code, Section II  
<line> (2) ASTM Standard  
<line> (3) Japanese Industrial Standard  
<line> 3) <**> Materials for Steel Structure  
<line> (1) ASME Boiler and Pressure Vessel Code, Section II  
<line> (2) ASTM Standard  
<line> (3) Japanese Industrial Standard  
<line> 2.4.2 <**> Others  
<P_line> 1) <**> Nondestructive Examination, ASME Section V, Latest Edition and Addenda.  
<line> 2) <**> Welding and brazing Qualifications, ASME Section IX, Latest Edition and 
<P_line> Addenda.  
<P_line> For code stamped vessels, ASME certified i.e. SA materials shall be used.  
<line> 2.4.3 <**> Pipe Size  
<line> 1) <**> ASME B36.10M Welded and Seamless Wrought Steel Pipe  
<line> 2) <**> ASME B36.19M Stainless Steel Pipe  
<line> 2.4.4 <**> Heat exchanger Tubes  
<line> 1) <**> SA-179  Seamless Cold-Drawn Low-Carbon Steel Heat-Exchanger and 
<P_line> Condenser Tubes.  
<Comma_line> 2) <**> SA-213  Seamless Ferritic and Austenitic Alloy-Steel Boiler, Superheater, 
<P_line> and Heat-Exchanger Tubes.  
<line> 3) <**> SA-334  Seamless and Welded Carbon and Alloy-Steel Tubes for Low-
<P_line> Temperature Service.  
<line> 4) <**> SB-338  Specification for Seamless and Welded Titanium and Titanium 
<P_line> Alloy Tubes for Condensers and Heat Exchangers.  
<line> 2.4.5 <**> Flanges and Fittings  
<line> 1) <**> 24�� and smaller: ASME B16.5   
<P_line> 2) <**> 26�� to 60��: ASME B 16.47 Series B, limited to class 300.  
<P_line> 3) <**> 62�� and larger: ASME VIII Div 1, Appendix 2.  
<P_line> 4) <**> Factory ? made Wrought Butt-Welding Fittings: ASME B16.9-2003.  
<P_line> 5) <**> Forged Steel Fittings, Socket Welding and Threaded: ASME B16.11-2005.  
<line> 2.4.6 <**> Threads  
<P_line> 1) <**> For pressure retaining flanged connection and for equipment anchor bolt.  
<line> (1) Threads: ASME B1.1  
<line> (2) Type of hexagon nut: ASME B18.2.2  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             6 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> (3) Height of nut: Heavy (identical to bolt diameter)  
<line> Note that up to and including 1�� shall be of UNC threading and that 1-1/8�� and 
<line> larger shall be of UN threading (8-thread series).  
<line> 2) <**> For internals  
<line> (1) Threads: ASME B1.1  
<line> (2) Type of hexagon nut: ASME B18.2.2  
<line> (3) Height of Nut: Standard  
<line> 3) <**> For platform/ladder and other structural parts  
<line> (1) Threads: ISO 68, ISO 261 and ISO 724 (ISO Metric)  
<line> (2) Type of hexagon Nut: ISO 4032  
<line> 4) <**> Pipe threads: ASME B1.20.1 NPT  
<line> 2.5 <**> Project Specifications  
<P_line> 1) <**> Criticality rating procedure for equipment GD-100-A4Z-5-02030.  
<P_line> 2) <**> Specification for Pressure Vessels GD-100-A4C-5-92001.  
<P_line> 3) <**> Specification for Air Coolers GD-100-A4E-5-02012.  
<P_line> 4) <**> Specification for Heavy Wall Pressure vessels GD-100-A4C-5-92005.  
<P_line> 5) <**> Specification for Shell and tube Heat exchangers GD-100-A4E-5-92010.  
<line> 6) <**> Specification for Protective Coatings for Equipment, Piping and Structures GD-100-
<P_line> A4Z-5-90013.  
<P_line> 7) <**> Specification for Equipment noise levels GD-100-A4Z-5-02027.  
<P_line> 8) <**> Specification for Seismic design GD-100-A4Q-5-98011.  
<line> 2.6 <**> Order of Precedence  
<Colon_line> In case of conflict, the order of precedence shall be:  
<line> 1) <**> Equipment Data Sheet(s)  
<line> 2) <**> Equipment Narrative Specification  
<line> 3) <**> Design General Specifications and Standards  
<line> 4) <**> Industry Codes and Standards 
<line> The above order of precedence is applicable to all interlink specifications in case when is 
<P_line> not specifically addressed.  
<line> 3. <**> UNIT OF MEASUREMENT  
<Colon_line> SI units shall be used as the official units except the following:  
<line> 1) <**>  Pipe size (inches, nominal)  
<line> 2) <**>  Flange rating (pound per square inch)  
<line> 3) <**>  Wire size (Birmingham Wire Gage)  
<line> 4) <**>  Anchor bolts (diameter in inches)  
<line> 5) <**>  Stud bolts (inches)  
<line> 4. <**> LANGUAGE  
<line> English shall be used. In addition to the original description in English, its translation in other 
<P_line> language may be shown thereto, if necessary.  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             7 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 5. <**> BASIC DESIGN DATA  
<line> 5.1 <**> Site Condition  
<line> 5.1.1 <**> Design Wind Load  
<line> Design wind loads shall not be less than those obtained from BS Code 6399 Part2 
<line> 1997 with the basic wind speed of 21 m/sec. For vessels with Height to Diameter 
<P_line> (H/D) ratio less than 10, the following simplified method may be used.  
<line> p = qs x Cpe x Ca  
<line> qs = 0.613 x Ve2  
<line> Ve = Vs x Sb 
<Semicolon_line> Where;  
<line> p = Design wind pressure (N/m2)  
<line> qs = dynamic pressure (N/m2)  
<line> Cpe = External pressure coefficient: 0.7 may be taken as a conservatively 
<line> simplified value  
<line> Ca = Size effect factor: 1.0 may be taken for all sizes  
<line> Ve = Effective wind speed (m/sec.)  
<P_line> Vs = Site wind speed: 21 m/sec.  
<line> Sb = Terrain and building factor: as per following table  
<table> Effective <|> Factor Sb <|> qs(N/m2) <|> p(N/m2)  
<line> height     
<line> He (m)     
<table> 2 or less <|> 1.48 <|> 650 <|> 455  
<table> 5 <|> 1.65 <|> 808 <|> 565  
<table> 10 <|> 1.78 <|> 940 <|> 658  
<table> 15 <|> 1.85 <|> 1015 <|> 711  
<table> 20 <|> 1.90 <|> 1071 <|> 750  
<table> 30 <|> 1.96 <|> 1140 <|> 798  
<table> 50 <|> 2.04 <|> 1235 <|> 864  
<table> 100 <|> 2.12 <|> 1333 <|> 933  
<P_line> Note: Interpolation may be used within each height.  
<line> Fw = p x De x L 
<Semicolon_line> Where;  
<line> Fw = Wind force (N)  
<line> De = Effective diameter at the height considered (m).  
<line> Effective diameter shall be taken as inside diameter, plus following amount, to 
<P_line> allow for platforms, insulation and piping.  
<line> Vessel with I.D. up to 2,500 mm; (I.D. + 600 mm)  
<line> Vessel with I.D. over 2,500 mm; (I.D. + 900 mm)  
<line> L = Length at the height considered (m).  
<line> For vessels with H/D ratio more than 10,  the following simplified method may be 
<P_line> used.  
<line> p = qs x Cpe x Ca  
<line> qs = 0.613 x Ve2  
<line> VE = Vs x Sb 
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             8 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<Colon_line> Where:  
<line> p = Design wind pressure (N/m2)  
<line> qs = dynamic pressure (N/m2)  
<line> Cpe = External pressure coefficient: 0.98 may be taken as a conservatively 
<line> simplified value  
<line> Ca = Size effect factor: 1.0 may be taken for all sizes  
<line> Ve = Effective wind speed (m/sec.)  
<P_line> Vs = Site wind speed: 21 m/sec.  
<line> Sb = Terrain and building factor: as per following table  
<table> Effective <|> Factor Sb <|> qs(N/m2) <|> p(N/m2)  
<line> height     
<line> He (m)    
<table> 2 or less <|> 1.48 <|> 650 <|> 637  
<table> 5 <|> 1.65 <|> 808 <|> 792  
<table> 10 <|> 1.78 <|> 940 <|> 921  
<table> 15 <|> 1.85 <|> 1015 <|> 995  
<table> 20 <|> 1.90 <|> 1071 <|> 1050  
<table> 30 <|> 1.96 <|> 1140 <|> 1117  
<table> 50 <|> 2.04 <|> 1235 <|> 1210  
<table> 100 <|> 2.12 <|> 1333 <|> 1307  
 
<P_line> Note: Interpolation may be used within each height.  
<line> Fw = p x De x L 
<Colon_line> Where:  
<line> Fw = Wind force (N)  
<line> De = Effective diameter at the height considered (m).  
<line> Effective diameter shall be taken as inside diameter, plus following amount, to 
<P_line> allow for platforms, insulation and piping.  
<line> Vessel with I.D. up to 2,500 mm; (I.D. + 600 mm)  
<line> Vessel with I.D. over 2,500 mm; (I.D. + 900 mm)  
<line> L = Length at the height considered (m).  
<line> 5.1.2 <**> Design Earthquake Load  
<line> 1) <**> Design earthquake load  
<Comma_line> The design earthquake load shall be as specified in GD-100-A4Q-5-98011, 
<line> ��Specification for Seismic Design��.  
<line> 2) <**> Vertical Seismic Loads  
<line> Vendor shall consider the Vertical Seismic Loads specified in GD-100-A4Q-
<P_line> 5-98011.  
<line> 3) <**> Overturning  
<line> Equipment and its anchor bolts shall be designed to resist the overturning 
<line> effect caused by the earthquake load as specified in the above (1).   
<line> Anchor bolt sizing shall also include combined effect of seismic bending 
<P_line> moment and seismic shear.  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             9 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 5.2 <**> Base Elevation (Above Finished Grade)  
<line> 1) <**>  Unless otherwise specified in individual equipment specification sheets, the 
<Semicolon_line> elevation of the equipment foundation above the finished grade shall be as follows;  
<line> Item  Minimum Foundation Elevation (mm)  
<line> Vertical Vessels  150  
<line> Horizontal Vessels  300  
<line> S/T Heat Exchanger  300  
<line> 2) <**>  For horizontal vessels and heat exchangers, in principle, the height of the 
<line> foundation should be increased to suit the required equipment elevation so that the 
<P_line> standard saddle height is kept intact.  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             10 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 6. <**> DETAILED DESIGN DATA  
<line> 6.1 <**> Pressure Vessel - General  
<line> 6.1.1 <**> Minimum Wall Thickness  
<line> To prevent deformation or damage during transportation and handling, no vessel 
<Comma_line> shall have a wall thickness less than that shown in the table below,  
 Vessel Diameter (mm) Min. Wall Thickness (mm)  
<line> 1) <**> Carbon steel & Low alloy steel (less than 10% Cr)  
<line> up to 2,500  6  
<line> 2,501 to 3,700  8  
<line> 3,701 to 4,300   9  
<line> 4,301 to 4,900  10  
<line> 4,901 to 6,100 12  
<line> 6,101 to 6,700 13  
<line> 6,701 to 7,200 14  
<line> 2) <**> High alloy steel (equal and more than 10% Cr)  
 any diameter  4  
<line> 3) <**> Non-ferrous metal  
 any diameter  4  
<line> 6.1.2 <**> Corrosion Allowance  
<line> Unless otherwise specified in individual equipment data sheets, the following 
<Comma_line> corrosion allowance shall be taken,  
<line> Type of Material  Min. Corrosion Allowance (mm)  
<line> 1) <**> Carbon steel   2.0  
<line> 2) <**> Low alloy steel   2.0  
<line> 3) <**> High alloy steel  no corrosion allowance required  
<line> 4) <**> Non-ferrous metal  no corrosion allowance required  
<line> 6.1.3 <**> Minimum Manhole Size  
<Colon_line> Minimum size of manhole for Column, Reactor and Drum shall be as follows:  
<line> Diameter (mm)  Min. Nominal Size  
<line> 1) <**> Less than 900  to provide a shell flange  
<line> 2) <**> 900 to 1,199   20��  
<line> 3) <**> 1,200 and larger   24��  
<line> For trayed columns manways shall be provided as follows: ? 
<line> 1) <**> One above the top tray  
<line> 2) <**> One below the bottom tray  
<line> 3) <**> An intermediate at every 10th tray  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             11 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 4) <**> One at transition sections and/or where internal distributors/liquid collectors are 
<P_line> located.  
<line> 6.1.4 <**> Heavy Wall Vessels  
<line> Vessels with wall thickness of 50 mm and above shall also meet the requirements 
<line> of specification GD-100-A4C-5-92005; ��Heavy Wall Pressure Vessels��  
<line> 6.1.5 <**> Vessels in Sour Service  
<line> Vessels in wet sour service shall also meet the requirements of specification 
<line> GD?100-A4C-5-92001; ��Pressure Vessels��  
<line> 6.2 <**> Shell and tube Type Heat Exchangers  
<line> 6.2.1 <**> Standard Tube Size  
<line> 1) <**> Tube length  
<line> Except for steam surface condensers and vertical thermosyphon reboilers, the 
<Comma_line> following tube length shall be considered as standard: 3,048 mm, 3,658 mm, 
<line> 4,878 mm, 6,096 mm, 7,315 mm, 9,754 mm, 12,192 mm  
<line> 2) <**> Tube Diameter and Wall Thickness  
<line> Unless otherwise specified in individual equipment data sheet, bare tubes shall 
<line> be 19.05 mm outside diameter. Where tubes larger than 19.05 mm OD are 
<line> proposed for special process reasons, these should be 25.4 mm OD. The 
<Colon_line> minimum tube wall thickness shall be as follows:  
<line> Material  Minimum Wall Thickness  
 19.05mm OD  25.4mm OD  
<table> (1) Carbon Steel <|> 2.11 mm <|> 2.77 mm  
<table> (2) Low alloy Steel (less than 10% Cr) <|> 2.11 mm <|> 2.77 mm  
<line> (3) High alloy Steel (equal and more than 10% Cr)1.65 mm  2.11 mm  
<table> (4) None- ferrous metal <|> 1.65 mm <|> 2.11 mm  
<table> (5) Monel <|> 1.24 mm <|> ---- 
<table> (6) Titanium <|> 0.914 mm <|> ---- 
<line> For low fin tubes, the above minimum wall thickness shall be provided at the 
<P_line> root of fin section. No land (plain section) at baffle hole is necessary.  
<line> 6.2.2 <**> Maximum Tube Bundle Weight  
<line> No removable tube bundle shall have a diameter greater than 1,500 mm and / or a 
<P_line> weight heavier than 25,000 kg.  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             12 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 6.2.3 <**> Tube Layout  
<line> 1) <**> Tubes on triangular pitch shall be used when the shell fluid has a low-fouling 
<line> tendency and any deposits can be removed by either water flushing or by 
<line> chemical means. If solids or corrosion deposits occur and cannot readily be 
<P_line> removed by chemical cleaning, square pitch shall be used.  
<line> (1) For fixed tubesheet exchangers, tubes shall be on a triangular or rotated 
<line> triangular pitch, since mechanical cleaning will not be required if this 
<P_line> design is considered suitable.  
<line> (2) Rotated square pitch may be adopted for viscous flow or condensing 
<P_line> service.  
<line> 2) <**> The number of tubes in any pass shall be within 10 % of the average number of 
<P_line> tubes per pass.  
<line> 6.2.4 <**> Entrainment Ratio  
<line> For design of kettle type vaporizers, the maximum allowable entrainment ratio shall 
<line> be 1.0 wt%.  
<line> 6.3 <**> Air Cooled Exchangers  
<line> 6.3.1 <**> Type of Draft   
<line> Unless specified in individual equipment data sheets, air cooled exchangers shall 
<P_line> be of forced draft type.  
<line> 6.3.2 <**> Type of Header   
<P_line> Unless specified in individual equipment data sheets, header shall be of plug type.  
<line> 6.3.3 <**> Tube Size  
<line> 1) <**> Tube Length  
<Comma_line> Straight tube lengths should be of 9,144 mm. If required by a specific design, 
<line> the use of 7,315 mm, 12,192 mm, 15,240 mm or other length may be proposed 
<P_line> for approval.  
<P_line> 2) <**> Minimum Tube O.D.  
<line> Unless specified in individual equipment data sheets, the minimum tube outside 
<P_line> diameter shall be 25.4 mm. Oval tubes shall not be adopted.  
<line> 3) <**> Minimum Tube Wall Thickness  
<Comma_line> The Minimum tube thickness shall be as follows,  
<line> (1) Carbon steel and low alloy steel (less than 10% Cr):  2.11 mm  
<line> (2) High alloy steel (equal and more than 10% Cr):  1.65 mm  
<line> 6.3.4 <**> Drive Arrangement   
<line> Unless specified in individual equipment data sheets, the drive arrangement shall 
<line> be of suspended v-belt drive, motor shaft up. A motor shall have a rain cover at the 
<P_line> drive end shaft.  
<line> 6.3.5 <**> Maximum Tube Bundle Size  
<line> 1) <**> Maximum width:  4,000 mm  
<line> 2) <**> Maximum weight:  20,000 kg  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             13 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 6.3.6 <**> Vibration Switch  
<line> A vibration switch shall be provided for each motor. Switch shall be set to open on 
<line> high vibration level. Switch reset shall be accessible from the maintenance platform 
<P_line> under the air cooler.  
<line> The switch shall be provided with suitable weather protection and to the 
<line> certification/rating class specified within the equipment data sheets. The switch 
<line> shall be certified for use in the specified electrical area classification. Vendor shall 
<line> confirm in his quotation method(s) by which switch(es) will be inspected, tested and 
<P_line> certified prior to delivery.  
<line> 6.4 <**> Noise Control  
<line> 6.4.1 <**> Permissible Noise Level  
<P_line> Permissible noise level shall be determined as per GD-100-A4Z-5-02027.  
<line> 6.4.2 <**> Noise Measurement Location   
<line> Locations for measuring the noise levels of equipment and installations be as 
<Comma_line> follows,  
<line> Item  Measurement Location  
<line> Air Cooled Exchangers  The noise level shall be measured 1 meter from the 
<P_line> fan ring at one fan operation.  
<line> Vacuum Equipment  The noise level shall be measured 1 meter from the 
<P_line> equipment surface and accessories.  
<line> Miscellaneous Equipment  The noise level shall be measured 1 meter from the 
<P_line> equipment surface and accessories.  
<P_line> 6.5 <**> Criticality Ratings.  
<P_line> Criticality Ratings for equipment shall be as per specification GD-100-A4Z-5-02030.   
<P_line> 6.6 <**> Thermal Insulation.  
<line> Thermal insulation shall be applied in accordance with GD-100-A4L-5-94049, GD-100-A4L-
<P_line> 5-94051, and GD-100-A4L-5-0155. 
<line> 6.7 <**> Fireproofing  
<line> 6.7.1 <**> Material  
<line> Mortar (Portland cement)  
<line> 6.7.2 <**> Bulk Density for Load Calculation  
<line> 2,200 kg/m3  
<line> 6.7.3 <**> Thickness  
<line> 50 mm  
 Doc. No. : GD-100-A4C-5-92002 
 Rev. No. :               0 
<line> Design Criteria for Pressure 
<line> Vessels, Shell & Tube 
<line> Exchangers and Air Coolers Sheet   :             14 / 14   
 
<line> Project  No : 5239 & 5279 
<line> ADGAS / IGD-E Das Island Project 
<line> GD-100-A4C-5-92002_R0.doc 
 
<line> 6.7.4 <**> Application  
<line> Unless specified in individual equipment data sheets, skirt supports shall be 
<P_line> fireproofed from outside only except when a flange is installed inside of skirts. 
<line> Saddle supports for horizontal vessels need not to be fireproofed unless the saddle 
<P_line> height exceeds 0.46 m.  
<line> 6.8 <**> Shop Painting  
<line> Shop painting shall be in accordance with the shop painting system shown in 
<line> GD-100-A4Z-5-90013, Specification for Protective Coatings for Equipment, Piping and 
<P_line> Structures.  
 

